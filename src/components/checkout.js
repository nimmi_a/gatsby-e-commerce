import React from "react"
import { loadStripe } from "@stripe/stripe-js"
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});
const buttonStyles = {
  fontSize: "13px",
  textAlign: "center",
  color: "#000",
  padding: "12px 60px",
  boxShadow: "2px 5px 10px rgba(0,0,0,.1)",
  backgroundColor: "rgb(255, 178, 56)",
  borderRadius: "6px",
  letterSpacing: "1.5px",
}
const stripePromise = loadStripe("pk_test_51HA9pUJD4TfJ1Aiwcv82k1slu2mg6IpTcVsewoD236aw419UIPRAKGJBnvRV8TENFI1iWMHukuW5I3sGGXzrbebE00ys2KFa94")
const redirectToCheckout = async event => {
    console.log("=================",event);
  event.preventDefault()
  const stripe = await stripePromise
  const { error } = await stripe.redirectToCheckout({
    lineItems: [{ price: "price_1GriHeAKu92npuros981EDUL", quantity: 1 }],
    mode: "payment",
    successUrl: `http://localhost:8000/page-2/`,
    cancelUrl: `http://localhost:8000/`,
  })
  if (error) {
    console.warn("Error:", error)
  }
}
const Checkout = () => {
    const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  return (
    <div> 

      <div>
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
Have a Look !!!        </Typography>
       
      </CardContent>
      <CardActions>
        <Button  onClick={redirectToCheckout} size="small">Click to find more product</Button>
      </CardActions>
    </Card>
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
         Have a Look !!!        </Typography>
       
      </CardContent>
      <CardActions>
        <Button  onClick={redirectToCheckout} size="small">Click to find more product</Button>
      </CardActions>
    </Card>
   
    </div>
     {/* <button style={buttonStyles} onClick={redirectToCheckout}>
     BUY MY BOOK
   </button> */}
</div>  )
}
export default Checkout