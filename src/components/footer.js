import React from "react"
import footerStyles from "./footer.module.scss"

const Footer = () => {
  return (
    <footer className={footerStyles.siteFooter}>
      <div className={footerStyles.container}>
    <div>
      <p>
        Site developed by Nimmi &copy; {new Date().getFullYear().toString()}{" "}
      </p>
    </div>
    </div>
    </footer>
  )
}
export default Footer