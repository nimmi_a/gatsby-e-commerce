import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { useState } from "react"
import Button from "@material-ui/core/Button"
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart"
import HeaderStyle from "./header.module.scss"
import LoginModal from "./Login/login.modal"

const Header = ({ siteTitle }) => {
  console.log()
  const HandleModal = e => {
    return <LoginModal visible={true} />
  }
  return (
    <div>
      <div className="{headerStyles.overlay}"></div>
      <header className={HeaderStyle.header}>
        <div>
          <h1 className={HeaderStyle.title}>{siteTitle}</h1>
        </div>
        <nav className={HeaderStyle.navContainer}>
          <ul className={HeaderStyle.navList}>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/productd">Products</Link>
            </li>
            <li>
              <Link to="/aboutus">About Us</Link>
            </li>
            <li className="icons-cart">
              <AddShoppingCartIcon />
            </li>
            <li>
              <Button variant="outlined" onClick={HandleModal}>
                Login
              </Button>
            </li>
          </ul>
        </nav>
      </header>
    </div>
  )

  Header.propTypes = {
    siteTitle: PropTypes.string,
  }

  Header.defaultProps = {
    siteTitle: ``,
  }
}

export default Header
