/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import "../styles/style.scss"
import Header from "./header"
import Footer from "./footer"
import LayoutStyles from "./layout.module.scss"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
    <div className={LayoutStyles.container}>
    <div className={LayoutStyles.content}>
      <Header siteTitle={data.site.siteMetadata.title} />
      <div className={LayoutStyles.mainContent}>        <main>{children}</main>
       
      </div>
      <Footer />
      </div>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
